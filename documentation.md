## Data Model

One-to-Many relationship with references :

-   we are not limited by the document size limit (16MB but still).
-   better write performances than embedded
-   pagination

```mermaid
classDiagram
      Topic <|-- Message
      class Topic{
          +ObjectId _id
          +String name
          +String description
          +Int created_at
      }
      class Message{
          +ObjectId _id
          +ObjectId topic
          +String created_at
          +String author
          +String message
      }
      class User {
          +ObjectId _id
          +String username
          +Array topics
      }
```
