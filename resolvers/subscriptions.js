import Apollo from "apollo-server";
const { withFilter } = Apollo;

export const SUBSCRIPTIONS = {
    NEW_MESSAGE: "new_message",
};

export default async function ({ pubsub }, x) {
    const messageAdded = {
        subscribe: withFilter(
            () => {
                return pubsub.asyncIterator([SUBSCRIPTIONS.NEW_MESSAGE]);
            },
            (payload, variables) => {
                return payload.messageAdded.topic === variables.topic;
            }
        ),
    };
    this.Subscription.messageAdded = messageAdded;
}
