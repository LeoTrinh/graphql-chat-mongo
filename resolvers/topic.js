export default async function (helpers) {
    this.Topic = {
        id: (root, _, __) => root._id,
        messages: (root, params, context) => {
            const {
                connectors: {
                    MongoDB: { db, COLLECTIONS },
                },
            } = context;

            return db
                .collection(COLLECTIONS.message)
                .find({ topic: root._id.toString() })
                .sort({ created_at: 1 })
                .toArray();
        },
        members: (root, _, context) => {
            const {
                connectors: {
                    MongoDB: { db, COLLECTIONS },
                },
            } = context;

            return db
                .collection(COLLECTIONS.user)
                .find({ topics: root._id.toString() })
                .toArray()
                .then((res) => {
                    return res.map((u) => u.username);
                });
        },
    };
}
