import { SUBSCRIPTIONS } from "./subscriptions.js";

export default async function (helpers) {
    async function createTopic(root, { topic }, context) {
        const {
            connectors: {
                MongoDB: { db, COLLECTIONS },
            },
        } = context;

        let entity = Object.assign({ created_at: Date.now() }, topic);

        return db
            .collection(COLLECTIONS.topic)
            .insertOne(entity)
            .catch((err) => {
                throw "Topic name already exists";
            })
            .then((res) => res.ops[0]);
    }

    // Topic can be an id or a name
    async function joinTopic(root, { topic }, context) {
        const {
            connectors: {
                MongoDB: { db, COLLECTIONS, ObjectId },
            },
            request: { user },
        } = context;

        let topicId;
        try {
            topicId = new ObjectId(topic);
        } catch (err) {
            // Nothing to do here
        }

        let topicEntity = await db
            .collection(COLLECTIONS.topic)
            .find({ $or: [{ name: topic }, { _id: topicId }] })
            .next();

        if (!topicEntity) {
            throw "Topic not found";
        }

        try {
            await addTopicToUserTopics.call(context, user.username, topicEntity, true);
        } catch (error) {
            throw error;
            // throw "Couldn't add topic to user's topics";
        }

        return topicEntity;
    }

    async function leaveTopic(root, { topic }, context) {
        const {
            connectors: {
                MongoDB: { db, COLLECTIONS, ObjectId },
            },
            request: { user },
        } = context;

        return db
            .collection(COLLECTIONS.user)
            .updateOne({ username: user.username }, { $pull: { topics: topic } })
            .then((res) => true);
    }

    async function postMessage(root, { topic, message }, context) {
        const {
            connectors: {
                MongoDB: { db, ObjectId, COLLECTIONS },
            },
            request: { user },
            pubsub,
        } = context;

        // First check topic existence
        let topicE = await db
            .collection(COLLECTIONS.topic)
            .find({ _id: new ObjectId(topic) })
            .next();

        if (!topicE) throw "Topic not found";

        // Then check if user has joined topic
        let userTopics = await db
            .collection(COLLECTIONS.user)
            .find({ username: user.username })
            .next()
            .then((res) => res.topics);

        if (!userTopics.includes(topic)) {
            throw "User is not allowed to post on this topic.";
        }

        // Finally create the message
        let entity = {
            message,
            topic,
            author: user.username,
            created_at: Date.now(),
        };

        return db
            .collection(COLLECTIONS.message)
            .insertOne(entity)
            .catch((err) => {
                throw "Message not posted";
            })
            .then((res) => {
                let msg = res.ops[0];
                pubsub.publish(SUBSCRIPTIONS.NEW_MESSAGE, { messageAdded: msg });

                return msg;
            });
    }

    async function inviteToTopic(root, { topic, usernames }, context) {
        const {
            connectors: {
                MongoDB: { db, ObjectId, COLLECTIONS },
            },
            request: { user },
        } = context;

        let topicId;
        try {
            topicId = new ObjectId(topic);
        } catch (err) {
            // Nothing to do here
        }

        // Check topic
        let topicEntity = await db
            .collection(COLLECTIONS.topic)
            .find({ $or: [{ name: topic }, { _id: topicId }] })
            .next();

        if (!topicEntity) {
            throw "Topic not found";
        }

        // Check user inviting is himself in the topic
        let host = await db.collection(COLLECTIONS.user).find({ username: user.username }).next();
        if (!host.topics.includes(topicEntity._id.toString())) {
            throw "Not allowed to invite someone";
        }

        // Finally invite people
        let promises = [];
        for (let username of usernames) {
            promises.push(addTopicToUserTopics.call(context, username, topicEntity));
        }

        await Promise.all(promises);
        return topicEntity;
    }

    async function addTopicToUserTopics(username, topicEntity, allowUserCreation = false) {
        const {
            connectors: {
                MongoDB: { db, COLLECTIONS },
            },
        } = this;

        let u = await db.collection(COLLECTIONS.user).find({ username }).next();

        if (!u) {
            if (allowUserCreation) {
                return db
                    .collection(COLLECTIONS.user)
                    .insertOne({ username, topics: [topicEntity._id.toString()] });
            } else {
                throw `User '${username}' does not exist`;
            }
        } else {
            return db
                .collection(COLLECTIONS.user)
                .updateOne({ username }, { $addToSet: { topics: topicEntity._id.toString() } });
        }
    }

    this.Mutation.createTopic = createTopic;
    this.Mutation.postMessage = postMessage;
    this.Mutation.joinTopic = joinTopic;
    this.Mutation.leaveTopic = leaveTopic;
    this.Mutation.inviteToTopic = inviteToTopic;
}
