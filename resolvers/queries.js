export default async function (helpers) {
    async function messages(root, { topic, limit = 0 }, context) {
        const {
            connectors: {
                MongoDB: { db, COLLECTIONS, ObjectId },
            },
        } = context;

        return db
            .collection(COLLECTIONS.message)
            .find({ topic })
            .sort({ created_at: -1 })
            .limit(limit)
            .toArray();
    }

    async function myTopics(root, _, context) {
        const {
            connectors: {
                MongoDB: { db, COLLECTIONS, ObjectId },
            },
            request: { user },
        } = context;

        let u = await db.collection(COLLECTIONS.user).find({ username: user.username }).next();

        // If user is not in db, add it with no topics
        if (!u) {
            await db
                .collection(COLLECTIONS.user)
                .insertOne({ username: user.username, topics: [] })
                .catch((err) => {
                    throw "Couldn't add default topics to user";
                });
            return [];
        }

        let topicsId = u.topics.map((t) => new ObjectId(t));

        return await db
            .collection(COLLECTIONS.topic)
            .find({ _id: { $in: topicsId } })
            .toArray();
    }

    this.Query.messages = messages;
    this.Query.myTopics = myTopics;
}
