export default async function(helpers) {
    this.Message = {
        id: (root, _, __) => root._id,
        created_at: (root, _, __) => root.created_at.toString()
    };
}
