FROM node:alpine

EXPOSE 3000
WORKDIR /app
CMD [ "npm", "start" ]

RUN apk update \
    && apk add python make g++

COPY ./package.json /app/package.json
RUN npm install

COPY . /app
