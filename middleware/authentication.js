import jwt from "jsonwebtoken";

export default async function middlewareWrapper(context) {
    return async function authenticate(req, res, next) {
        context.logger.debug("Authentication challenge");
        if (req.headers.authorization) {
            let token = req.headers.authorization.slice(7) || "none";

            await new Promise((resolve, reject) => {
                jwt.verify(token, process.env.JWT_SECRET, (err, decoded) => {
                    err && reject(err);
                    decoded && resolve(decoded);
                });
            })
                .then((user) => {
                    req.user = user;
                    return next();
                })
                .catch((err) => {
                    context.logger.debug("Authentication failed :", req.headers.authorization);
                    res.status(401).json("Unauthorized");
                });
        } else {
            res.status(401).json();
        }
    };
}
