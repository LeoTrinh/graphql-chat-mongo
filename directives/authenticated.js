import GraphQlTools from "graphql-tools";

class AuthDirective extends GraphQlTools.SchemaDirectiveVisitor {
    static name = "authenticated";

    visitFieldDefinition(field, { objectType }) {
        const privilege = this.args.privilege || "user";
        const { resolve = defaultFieldResolver } = field;

        field.resolve = async function (parent, params, context) {
            if (!context.request.user) throw "Unauthorized";

            return resolve.call(this, parent, params, context);
        };
    }
}

export default AuthDirective;
