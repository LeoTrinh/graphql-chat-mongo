import { start } from "@lymeodev/apollon";
import dotenv from "dotenv";

dotenv.config();
start.fromUrl(import.meta.url);
