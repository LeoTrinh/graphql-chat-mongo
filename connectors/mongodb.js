import Mongo from "mongodb";

export default async function MongoDB() {
    const logger = this.logger;
    const { MONGO_HOST, MONGO_PORT, DB_NAME, MONGO_USERNAME, MONGO_PASSWORD } = process.env;
    const COLLECTIONS = {
        topic: "topic",
        message: "message",
        user: "user",
    };

    const mongoURL = MONGO_USERNAME
        ? `mongodb://${MONGO_USERNAME}:${MONGO_PASSWORD}@${MONGO_HOST}:${MONGO_PORT}`
        : `mongodb://${MONGO_HOST}:${MONGO_PORT}`;

    async function connect() {
        let client;
        try {
            client = await Mongo.MongoClient.connect(mongoURL);
        } catch (err) {
            logger.warn(`Server link failed : ${err}`);
            return await reject();
        }
        let db = client.db(DB_NAME);

        await setUpCollections.call(this, db, COLLECTIONS);
        return { client, db };
    }

    async function reject() {
        await new Promise((resolve) => setTimeout(resolve, 5000));
        return await connect();
    }

    try {
        let { client, db } = await connect();
        return {
            client,
            db,
            ObjectId: Mongo.ObjectId,
            getNewObjectID,
            COLLECTIONS,
        };
    } catch (error) {
        this.logger.error("MongoDb Error :", error);
    }
}

async function setUpCollections(db, collections) {
    let schemas = {
        topic: {
            schema: {
                $jsonSchema: {
                    bsonType: "object",
                    required: ["name"],
                    properties: {
                        name: {
                            bsonType: "string",
                            description: "must be a string and is required",
                        },
                    },
                },
            },
            index: {
                properties: { name: 1 },
                options: { unique: true },
            },
        },
        message: {
            schema: {
                $jsonSchema: {
                    bsonType: "object",
                    required: ["topic", "author"],
                    properties: {
                        topic: {
                            bsonType: "string",
                            description: "must be a string and is required",
                        },
                        author: {
                            bsonType: "string",
                            description: "must be a string and is required",
                        },
                        created_at: {
                            bsonType: "timestamp",
                            description: "must be a string and is required",
                        },
                    },
                },
            },
        },
        // user: {
        //     schema: {
        //         $jsonSchema: {
        //             bsonType: "object",
        //             required: ["username"],
        //             properties: {
        //                 username: {
        //                     bsonType: "string",
        //                     description: "must be a string and is required"
        //                 },
        //                 topics: {
        //                     bsonType: "array"
        //                 }
        //             }
        //         }
        //     }
        // }
    };

    let promises = [];
    for (let col in collections) {
        if (schemas[col]) {
            promises.push(
                db
                    .createCollection(col, {
                        validator: schemas[col].schema,
                        validationLevel: "off",
                    })
                    .catch((err) => this.logger.info(`Collection ${col} already exists`))
            );

            if (schemas[col].index) {
                promises.push(
                    db
                        .collection(col)
                        .createIndex(schemas[col].index.properties, schemas[col].index.options)
                        .catch((err) => this.logger.info(`Index already exists`))
                );
            }
        }
    }
    await Promise.all(promises).catch((err) => {
        throw err;
    });
    return;
}

function getNewObjectID() {
    return new Mongo.ObjectID();
}
